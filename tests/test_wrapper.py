import pytest
from matrix_bot._exceptions import _wrap_errors_into_exceptions


class ErrorResponse:
    pass

class ErrorResponseA(ErrorResponse):
    pass

class SomeMixin:
    pass

class ErrorResponseB(ErrorResponseA, SomeMixin):
    pass

@pytest.fixture
def error_map():
    return dict(_wrap_errors_into_exceptions(
        ErrorResponse,
        {
            "Does": ErrorResponse,
            "Not": ErrorResponseA,
            "Matter": ErrorResponseB
        },
    ))

def test_make_wrappers_returns_a_mapping_of_original_class_to_exception(error_map):
    for new in error_map.values():
        assert issubclass(new, Exception)

def test_make_wrappers_preserves_some_sort_of_hierarchy(error_map):
    assert issubclass(error_map[ErrorResponseA], Exception)
    assert issubclass(error_map[ErrorResponseA], error_map[ErrorResponse])
    assert issubclass(error_map[ErrorResponseB], error_map[ErrorResponseA])
