import pytest
from matrix_bot._client import make_proxy


class ErrorResult:
    pass

class SuccessResult:
    pass

class ErrorResultException(Exception):
    pass

class DummyClass:
    value = 123
    async def async_method_success(self):
        return SuccessResult()

    async def async_method_fail(self):
        return ErrorResult()

    def sync_method_success(self):
        return SuccessResult()
    
    def sync_method_fail(self):
        return ErrorResult()

    def sync_returning_awaitable_success(self):
        return self.async_method_success()
    
    def sync_returning_awaitable_fail(self):
        return self.async_method_fail()

ProxyDummyClass = make_proxy(DummyClass, {
    ErrorResult: ErrorResultException
})

@pytest.fixture
def wrapped_instance():
    return ProxyDummyClass()

@pytest.mark.asyncio
async def test_async_error_raises(wrapped_instance):

    with pytest.raises(ErrorResultException):
        await wrapped_instance.async_method_fail()

@pytest.mark.asyncio
async def test_async_success_does_not_raise(wrapped_instance):
    assert isinstance(await wrapped_instance.async_method_success(), SuccessResult)

@pytest.mark.asyncio
async def test_sync_returning_awaitable_error_raises(wrapped_instance):

    with pytest.raises(ErrorResultException):
        await wrapped_instance.sync_returning_awaitable_fail()

@pytest.mark.asyncio
async def test_sync_returning_awaitable_success_does_not_raise(wrapped_instance):
    assert isinstance(await wrapped_instance.sync_returning_awaitable_success(), SuccessResult)

def test_sync_error_raises(wrapped_instance):
    with pytest.raises(ErrorResultException):
        wrapped_instance.sync_method_fail()

def test_sync_success_does_not_raise(wrapped_instance):

    assert isinstance(wrapped_instance.sync_method_success(), SuccessResult)

def test_attribute_access_works(wrapped_instance):
    assert wrapped_instance.value == 123
