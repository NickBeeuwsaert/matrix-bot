# Matrix bot

A matrix bot framework I guess. I mean I started out trying to write a welcome bot. But now it does several bots possibly.

## Setup

I guess get a development matrix server because odds are this will create a thousand DM rooms on accident

Update config.toml to point to your matrix server and the username of your bot

Using poetry to create the development environment:

```
poetry install

# Do initial login to get access token
poetry run matrix-bot login

# Run the bot
poetry run matrix-bot run
```
