from collections.abc import Mapping
from functools import singledispatch


@singledispatch
def _flatten(value, path):
    yield (path, value)


@_flatten.register(Mapping)
def _flatten_dict(mapping, path):
    for key, value in mapping.items():
        yield from _flatten(value, (*path, key))


def flatten(v, joiner: str = "."):
    return {joiner.join(key): value for key, value in _flatten(v, ())}
