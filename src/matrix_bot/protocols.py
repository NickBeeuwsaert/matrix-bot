from typing import Protocol

from nio import AsyncClient


class Plugin(Protocol):
    def __init__(self, client: AsyncClient):
        pass

    def enable(self):
        """Enable the extension."""

    def disable(self):
        """Disable the extension."""
