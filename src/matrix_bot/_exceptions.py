"""
I'm not sure why, but nio-matrix doesn't raise exceptions, which makes it
really annoying to check for errors. So, I am going to force it to raise
exceptions, whether it likes it or not
"""
import inspect
from collections.abc import Mapping

from nio import responses
from nio.responses import *


def _wrapper_init(self, original):
    Exception.__init__(self, original)
    self.original = original

class _ExceptionMapping(Mapping):
    def __init__(self, base_class):
        self._base_class = base_class
        self._backing_store = {
            base_class: type(base_class.__name__, (Exception, ), dict(__init__=_wrapper_init))
        }

    def __len__(self):
        return len(self._backing_store)
    
    def __iter__(self):
        return iter(self._backing_store)

    def __getitem__(self, key: type):
        try:
            return self._backing_store[key]
        except KeyError:
            return self._backing_store.setdefault(
                key,
                type(key.__name__,
                    tuple(
                        self[base]
                        for base in key.__bases__
                        if issubclass(base, self._base_class)
                    ),
                    {}
                )
            )

def _wrap_errors_into_exceptions(error_base, possible_children):
    exception_mapping = _ExceptionMapping(error_base)
    for error_class in possible_children.values():
        if inspect.isclass(error_class) and issubclass(error_class, error_base):
            yield error_class, exception_mapping[error_class]


exception_mapping = dict(_wrap_errors_into_exceptions(responses.ErrorResponse, vars(responses)))
locals().update({
    exception.__name__: exception
    for exception in exception_mapping.values()
})
# _exception_mapping = _ExceptionMapping(responses.ErrorResponse)
# for error_class in vars(responses).values():
#     if inspect.isclass(error_class) and issubclass(error_class, responses.ErrorResponse):
#         locals().setdefault(error_class.__name__, _exception_mapping[error_class])
#     del error_class
# exception_mapping = dict(_exception_mapping)
