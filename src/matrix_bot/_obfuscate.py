"""
Write data in an obfuscated format to disk.

This is by no means secure, we are just using gzip. The point is to protect
users who are live coding and setting up the bot from accidentally revealing
their tokens on stream::

    $ cat secrets.json
    {
        "access_token": "OOPS"
    }

If theres a better way to do this (and there probably is) I'm all ears.

I am so tired right now
"""

import gzip
import json
from pathlib import Path


def write_obfuscated(path: Path, data):
    path = Path(path)
    with path.open('wb') as fp:
        fp.write(gzip.compress(json.dumps(data).encode()))

def read_obfuscated(path: Path):
    path = Path(path)
    with path.open('rb') as fp:
        return json.loads(gzip.decompress(fp.read()).decode())
