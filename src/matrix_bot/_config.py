from pathlib import Path

import toml

from ._flatten import flatten


def toml_config(path):
    with Path(path).open('r') as fp:
        return flatten(toml.load(fp))
