import asyncio
import sys
from argparse import ArgumentParser
from importlib.metadata import entry_points as get_entry_points
from pathlib import Path

from nio import RoomVisibility
from nio.events.room_events import RoomMemberEvent

from .._client import AsyncClient
from .._config import toml_config
from .._exceptions import RoomCreateError
from .._obfuscate import read_obfuscated

parser = ArgumentParser('matrix-bot login')
parser.add_argument('-c', '--config', default="config.toml", type=toml_config)

def get_plugins(enabled_plugins):
    entry_points = get_entry_points()["matrix-bot.plugins"]
    for entry_point in entry_points:
        if entry_point.name in enabled_plugins:
            yield entry_point.load()


async def _main(config):
    secret_file = Path(config.get("matrix.secret_file", "./secrets.json.gz"))
    client = AsyncClient(config["matrix.homeserver"])
    plugins = [
        plugin(client)
        for plugin in get_plugins(config.get("matrix-bot.plugins", []))
    ]
    if not secret_file.exists():
        command, *_ = sys.argv
        print(f"No secret file exists! Run `{command} login`!")
        return -1
    
    secret = read_obfuscated(secret_file)
    client.user_id = config["matrix.username"]
    client.access_token = secret["token"]
    client.device_id = secret["device"]


    for plugin in plugins:
        print(f"Enabling {plugin=}")
        await plugin.enable()

    try:
        await client.sync_forever()
    except KeyboardInterrupt:
        # This doesnt really do anything if the interrupt occurs in a callback
        print("Stopping...")
    finally:

        for plugin in plugins:
            await plugin.disable()
        await client.close()


def main(argv=None):
    args = parser.parse_args(argv)
    sys.exit(asyncio.run(_main(args.config)))

if __name__ == "__main__":
    main()
