import asyncio
import sys
from argparse import ArgumentParser
from getpass import getpass
from operator import attrgetter
from pathlib import Path

from .._client import AsyncClient
from .._config import toml_config
from .._exceptions import LoginError
from .._obfuscate import write_obfuscated

accept_responses = {"y", "yes", "yep", "sure", "uh-huh", "maybe", "why not", "ja", "totally", "totes"}
reject_responses = {"n", "no", "nope", "nah", "nuh-uh", "probably not", "nein"}
get_secrets = attrgetter("access_token", "device_id")

parser = ArgumentParser('matrix-bot login')
parser.add_argument('-c', '--config', default="config.toml", type=toml_config)
parser.add_argument('-f', '--force', action='store_true', default=False)

async def _main(config, secret_file, password):
    client = AsyncClient(config["matrix.homeserver"], config["matrix.username"])

    try:
        response = await client.login(password)
    except LoginError as e:
        print("Problem logging in!")
        print(e.original)
        return -1
    else:
        write_obfuscated(secret_file, dict(
            zip(("token", "device"), get_secrets(response))
        ))
        print(f"Wrote secrets to {secret_file!r}")
    finally:
        await client.close()
    return 0

def main(argv=None):
    args = parser.parse_args(argv)
    secret_file = Path(args.config.get("matrix.secret_file", "./secrets.json.gz"))

    if secret_file.exists() and not args.force:
        print(f"{secret_file!r} already exists! Overwrite? ")
        while (answer := input("[y/n]: ").lower().strip()) not in accept_responses | reject_responses:
            pass

        if answer in reject_responses:
            sys.exit(-1)

    password = getpass("Password: ")
    sys.exit(asyncio.run(_main(args.config, secret_file, password)))

if __name__ == "__main__":
    main()
