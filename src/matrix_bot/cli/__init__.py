import argparse
from argparse import ArgumentParser
from importlib.metadata import entry_points as get_entry_points
from pathlib import Path
from typing import List, Optional

parser = ArgumentParser()
# The argpare.PARSER nargs argument is undocumented, so this might break in the
# future. But the recommended way of doing subcommands in argparse is terrible.
# The intent here is that extension authors just need to write a main(args)
# function, and be free to use *whichever* argument parsing library they want
# (e.g.: argparse, docopt, click, ...)
# Also, argparse.PARSER makes argparse act like BSD-getopt. Which is far superior
# to GNU-getopt. Fight me.
parser.add_argument(
    "command",
    metavar="COMMAND",
    nargs=argparse.PARSER,
)
parser.add_argument('-c', '--config', type=Path, default="./config.toml")


def populate_optional_args(args, default_config):
    parser = ArgumentParser()
    parser.add_argument('-c', '--config', type=Path, default=default_config)

    parsed, unparsed = parser.parse_known_args(args)

    if parsed.config:
        unparsed = [f'--config={parsed.config}'] + unparsed
    return unparsed

def main(argv: Optional[List[str]] = None):
    args = parser.parse_args(argv)

    command, *command_args = args.command


    entry_points = {
        ep.name: ep.load()
        for ep in get_entry_points()["matrix-bot.cli"]
    }
    entry_points[command](populate_optional_args(command_args, args.config))
