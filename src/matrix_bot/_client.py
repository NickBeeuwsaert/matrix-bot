"""
Wrap nio's async client to raise exceptions instead of returning errors.

This is probably a terrible idea
"""
import inspect
from asyncio import (CancelledError, InvalidStateError, create_task,
                     ensure_future, get_event_loop, iscoroutine,
                     iscoroutinefunction)
from collections.abc import Mapping
from functools import update_wrapper, wraps

from nio import AsyncClient as RealAsyncClient
from nio import responses

from ._exceptions import exception_mapping


class ProxyMethod:
    def __init__(self, fn, error_map):
        self._fn = fn
        self._error_map = error_map
        update_wrapper(self, self._fn)
    
    def _handle_result(self, value):
        try:
            raise self._error_map[type(value)](value)
        except KeyError:
            return value

    async def _async_handle_result(self, value):
        return self._handle_result(await value)

    def __call__(self, *args, **kwargs):
        result = self._fn(*args, **kwargs)

        if inspect.isawaitable(result):
            return self._async_handle_result(result)

        return self._handle_result(result)

def make_proxy(forward_class, error_map):
    class Wrapper:
        def __init__(self, *args, **kwargs):
            self._forward = forward_class(*args, **kwargs)

        def __getattr__(self, name):
            attr = getattr(self._forward, name)
            if callable(attr):
                # return raise_or_return(attr)
                return ProxyMethod(attr, error_map)
            return attr
    
        def __setattr__(self, name, value):
            if name == '_forward':
                super().__setattr__(name, value)
                return
            setattr(self._forward, name, value)

    Wrapper.__name__ = forward_class.__name__

    return Wrapper

AsyncClient = make_proxy(RealAsyncClient, exception_mapping)
