from dataclasses import dataclass

from matrix_bot.protocols import Plugin
from nio import AsyncClient


@dataclass
class WelcomeBot:
    """
    Ok, so the plan here is to listen to presence events maybe? I'm not sure
    if when a user joins for the first time the event will transition from some
    never-signed-in-state to online. Or, as a fallback, have a room all users
    are joined to implicitly and listen for join events there. I dunno I cant
    find a list of matrix event types.

    Also should hook a database up to this, so that users who have already gotten
    a message wont get subsequent messages (this is assuming that this bot is
    awesome and totally helpful. Which it will be. Probably.)
    """
    client: AsyncClient

    async def _presence_event(room, event):
        pass

    async def enable(self):
        await self.client.room_send(
            room_id="!fupwZviajbNXyrPhYI:matrix.bot-dev.local",
            message_type="m.room.message",
            content={
                "msgtype": "m.text",
                "body": "Hello, world"
            }
        )

    async def disable(self):
        # So, I assumed when I wrote this protocol that when you add a callback
        # you can remove a callback, but that doesnt seem to be the case
        # so I guess plugins will have to track their state and disable callbacks
        # on their own.
        pass

